import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FossilSearchByTagComponent } from './fossil-search-by-tag/fossil-search-by-tag.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModResultsComponent } from './mod-results/mod-results.component';

@NgModule({
  declarations: [
    AppComponent,
    FossilSearchByTagComponent,
    ModResultsComponent
  ],
  imports: [
    NgbModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
