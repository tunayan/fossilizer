export interface ModTemplate {
  name: string;
  description: string;
  affix: string;
  required_level: number;
  bases: string[];
  tags: string[];
}

export class Mod implements ModTemplate {
  affix: string;
  bases: string[];
  description: string;
  name: string;
  required_level: number;
  tags: string[];

  constructor(template: ModTemplate) {
    Object.assign(this, template);
  }

  get descriptionWithAffix() {
    const affix = this.affix === "prefix" ? "[P]" : "[S]";
    return `${affix} ${this.description}`;
  }

}
